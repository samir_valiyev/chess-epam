package com.epam.main;

import com.epam.table.Table;

import java.util.List;

public class Game {
    private static Game instance;
    private Table table;
    private List<Player> players;
    private Player activePlayer;

    private Game() {
    }

    public static Game getInstance() {

        if (instance == null) {
            instance = new Game();
        }
        return instance;
    }

    public void startGame() {

    }
    public void finishGame() {

    }
}
