package com.epam.table;

import com.epam.Drawable;
import com.epam.figure.Figure;
import com.epam.utilities.Vector;

;import java.awt.*;

public class Cell implements Drawable {
    private Vector position;
    private Figure occupiedBy;
    private Color color;

    @Override
    public void draw() {

    }

    public boolean isFree() {
        return occupiedBy == null;
    }
}
