package com.epam.figure;

import com.epam.Drawable;
import com.epam.table.Cell;
import com.epam.utilities.Vector;

public abstract class Figure implements Drawable {
    protected String name;
    protected int teamId;
    protected boolean isDead;
    protected Cell tookCell;

    public Figure(int teamId, Cell tookCell) {
        this.teamId = teamId;
        this.tookCell = tookCell;
    }

    public void tryToMove(Vector position) {

    }

    public boolean canMoveTo(Vector position) {
        return false;
    }

    private void moveTo(Vector position) {

    }

    public void onDead() {
    }

    public void onMoved() {
    }

    @Override
    public void draw() {

    }
}
