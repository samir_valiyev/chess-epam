package com.epam.figure.figures;

import com.epam.figure.Figure;
import com.epam.table.Cell;
import com.epam.utilities.Vector;


public class King extends Figure {

    public King(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "King";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        return false;
    }

    @Override
    public void onDead() {

    }

    @Override
    public void draw() {

    }
}
