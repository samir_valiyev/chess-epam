package com.epam.figure.figures;

import com.epam.figure.Figure;
import com.epam.table.Cell;
import com.epam.utilities.Vector;


public class Knight extends Figure {

    public Knight(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Knight";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        return false;
    }

    @Override
    public void draw() {

    }
}
