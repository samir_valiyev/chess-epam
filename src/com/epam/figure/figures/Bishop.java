package com.epam.figure.figures;

import com.epam.figure.Figure;
import com.epam.table.Cell;
import com.epam.utilities.Vector;


public class Bishop extends Figure {

    public Bishop(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Bishop";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        return false;
    }

    @Override
    public void draw() {

    }
}
