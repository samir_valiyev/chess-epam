package com.epam.figure.figures;

import com.epam.figure.Figure;
import com.epam.table.Cell;
import com.epam.utilities.Vector;


public class Pawn extends Figure {

    public Pawn(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Pawn";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        return false;
    }

    @Override
    public void onMoved() {
        super.onMoved();
    }

    @Override
    public void draw() {

    }
}
