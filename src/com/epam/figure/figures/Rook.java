package com.epam.figure.figures;

import com.epam.figure.Figure;
import com.epam.table.Cell;
import com.epam.utilities.Vector;


public class Rook extends Figure {

    public Rook(int teamId, Cell tookCell) {
        super(teamId, tookCell);
        this.name = "Rook";
    }

    @Override
    public boolean canMoveTo(Vector position) {
        return false;
    }

    @Override
    public void draw() {

    }
}
