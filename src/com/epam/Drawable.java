package com.epam;

public interface Drawable {
    void draw();
}
